// console.log("Hello World");

// [SECTION] Document Object Model
    // Allow us to be able to access or modify the properties of an html element in a webpage
    // It is a standard on how to get, change, add, or delete HTML elements.
    // We will focus on using DOM for managing forms.

    // For selecting HTML elements will be using the document.querySelector
    // Syntax: document.querySelector("htmlElement")
        // document refers to the whole page
        // ".querySelector" is used to select a specific object (HTML elements) from the document (web page).
        // The query selector function takes a string input that is formatted like a "CSS selector" when applying the styles.
       
        
    // Alternatively, we can use the getElement functions to retrieve the elements.
        // document.getElementById("htmlElement");


    // However, using these functions require us to identify beforehand how we get the elements. With querySelector, we can be flexible in how to retrieve the elements.

    const txtFirstName = document.querySelector("#txt-first-name");
    const txtLastName = document.querySelector("#txt-last-name");
    const spanFullname = document.querySelector("#span-full-name");



// [SECTION] Event Listeners
    // Whenever a user interacts with a web page, this action is considered as an event.
    // Working with events is large part of creating interactivity in a web page.
    // To perform an action when an event triggers, you first need to listen to it.

    // The method use is "addEventListener" that takes two arguments:
        // A string identifying an event;
        // and a function that the listener will execute once the "specified event" is triggered.
        // When an event occurs, an "event object" is passed to the function argument as the first parameter
    txtFirstName.addEventListener("keyup", (event) => {
        console.log(event.target); // contains the element
        console.log(event.target.value); //contains the actual value
    });

    txtFirstName.addEventListener("keyup", () => {

        // The "innerHTML" property sets or returns the HTML content (innerHTML) of an element (div, span, etc.)
        // The ".value" property sets or returns the value of an attribute. (form control elements: input, select, etc.)
        spanFullname.innerHTML = `${txtFirstName.value}`;
    });


    // Creating Multiple events that will trigger in a same function

    const fullName = () => {
        spanFullname.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
    };

    txtFirstName.addEventListener("keyup", fullName);
    txtLastName.addEventListener("keyup", fullName);



    //Activity s47
    /*
    1. Create an addEventListener that will "change the color" of the "spanFullName". 
    2. Add a "select tag" element with the options/values of red, green, and blue in your index.html.
    3. The values of the select tag will be use to change the color of the span with the id "spanFullName"

    */

  const textColor = document.querySelector("#text-color");


  textColor.addEventListener("click", () => {
    spanFullname.style.color = textColor.value;
  });
//   ==========================================================
    
  

  